/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pin.oxprogramoop;

/**
 *
 * @author USER
 */
public class Table {

    private char[][] table = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    private Player playerX;
    private Player playerO;
    private Player currentPlayer;
    private Player winner;
    private boolean finish = false;
    private int lastCol;
    private int lastRow;
    private int round;

    public Table(Player x, Player o) {
        playerX = x;
        playerO = o;
        currentPlayer = x;
        round = 1;
    }

    public void showTable() {
        System.out.println("  1 2 3");
        for (int i = 0; i < 3; i++) {
            System.out.print(i + 1 + " ");
            for (int j = 0; j < 3; j++) {
                System.out.print(table[i][j] + " ");
            }
            System.out.println("");
        }
    }

    public int getRound() {
        return round;
    }

    public Player getTurn() {
        if (this.getRound() % 2 == 1) {
            return playerX;
        }

        return playerO;
    }

    public int swichPlayer() {
        if (currentPlayer == playerX) {
            currentPlayer = playerO;
        } else {
            currentPlayer = playerX;
        }
        return round++;
    }

    public boolean checkTable(int row, int col) {
        if (this.getTable()[row][col] == '-') {
            return true;
        }

        return false;
    }

    public void setTable(int row, int col) {
        this.getTable()[row][col] = currentPlayer.getName();
        lastRow = row;
        lastCol = col;
    }

    public char[][] getTable() {
        return table;
    }

    public void checkWin() {
        checkRow();
        checkCol();
        checkX1();
        checkX2();
    }

    public void checkRow() {
        for (int col = 0; col < 3; col++) {
            if (table[lastRow][col] != currentPlayer.getName()) {
                return;

            }
        }
        finish = true;
        winner = currentPlayer;
        setStatWinLose();
    }

    public void checkCol() {
        for (int row = 0; row < 3; row++) {
            if (table[row][lastCol] != currentPlayer.getName()) {
                return;
            }
        }
        finish = true;
        winner = currentPlayer;
        setStatWinLose();
    }

    public void checkX1() {
        for (int i = 0; i < 3; i++) {
            if (table[i][i] != currentPlayer.getName()) {
                return;
            }
        }
        finish = true;
        winner = currentPlayer;
        setStatWinLose();
    }

    public void checkX2() {
        int colx = 2;
        for (int i = 0; i < 3; i++) {
            if (table[i][colx] != currentPlayer.getName()) {
                return;
            }
            colx--;
        }
        finish = true;
        winner = currentPlayer;
        setStatWinLose();
    }

    public void setStatWinLose() {
        if (currentPlayer == playerO) {
            playerO.win();
            playerX.lose();
        } else {
            playerO.lose();
            playerX.win();
        }
    }

    public boolean isFinish() {
        return finish;
    }

    public void setFinish() {
        finish = true;
    }

    public Player getWinner() {
        return winner;
    }

}
