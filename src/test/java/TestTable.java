/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.pin.oxprogramoop.Player;
import com.pin.oxprogramoop.Table;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author USER
 */
public class TestTable {

    public TestTable() {
    }

    @BeforeAll
    public static void setUpClass() {
    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {
    }

    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void testRow1ByX() {
        Player x = new Player('x');
        Player o = new Player('o');
        Table table = new Table(x, o);
        table.setTable(0, 0);
        table.setTable(0, 1);
        table.setTable(0, 2);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(x, table.getWinner());
    }

    @Test
    public void testRow2ByX() {
        Player x = new Player('x');
        Player o = new Player('o');
        Table table = new Table(x, o);
        table.setTable(1, 0);
        table.setTable(1, 1);
        table.setTable(1, 2);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(x, table.getWinner());
    }

    @Test
    public void testRow3ByX() {
        Player x = new Player('x');
        Player o = new Player('o');
        Table table = new Table(x, o);
        table.setTable(2, 0);
        table.setTable(2, 1);
        table.setTable(2, 2);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(x, table.getWinner());
    }
@Test
    public void testRow4ByX() {
        Player x = new Player('x');
        Player o = new Player('o');
        Table table = new Table(x, o);
        table.setTable(0, 0);
        table.setTable(1, 0);
        table.setTable(2, 0);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(x, table.getWinner());
    }
   @Test
    public void testRow5ByX() {
        Player x = new Player('x');
        Player o = new Player('o');
        Table table = new Table(x, o);
        table.setTable(0, 1);
        table.setTable(1, 1);
        table.setTable(2, 1);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(x, table.getWinner());
    } 
  @Test
    public void testRow6ByX() {
        Player x = new Player('x');
        Player o = new Player('o');
        Table table = new Table(x, o);
        table.setTable(0,2);
        table.setTable(1,2);
        table.setTable(2,2);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(x, table.getWinner());
    }   
     @Test
    public void testRow7ByX() {
        Player x = new Player('x');
        Player o = new Player('o');
        Table table = new Table(x, o);
        table.setTable(0, 0);
        table.setTable(1, 1);
        table.setTable(2,2);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(x, table.getWinner());
    }
     @Test
    public void testRow8ByX() {
        Player x = new Player('x');
        Player o = new Player('o');
        Table table = new Table(x, o);
        table.setTable(2, 2);
        table.setTable(1, 1);
        table.setTable(0,0);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(x, table.getWinner());
    }
     @Test
     public void testRow1ByO() {
        Player x = new Player('x');
        Player o = new Player('o');
        Table table = new Table(o, x);
        table.setTable(0, 0);
        table.setTable(0, 1);
        table.setTable(0, 2);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(o, table.getWinner());
    }

    @Test
    public void testRow2ByO() {
        Player x = new Player('x');
        Player o = new Player('o');
        Table table = new Table(o, x);
        table.setTable(1, 0);
        table.setTable(1, 1);
        table.setTable(1, 2);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(o, table.getWinner());
    }

    @Test
    public void testRow3ByO() {
        Player x = new Player('x');
        Player o = new Player('o');
        Table table = new Table(o, x);
        table.setTable(2, 0);
        table.setTable(2, 1);
        table.setTable(2, 2);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(o, table.getWinner());
    }
@Test
    public void testRow4ByO() {
        Player x = new Player('x');
        Player o = new Player('o');
        Table table = new Table(o, x);
        table.setTable(0, 0);
        table.setTable(1, 0);
        table.setTable(2, 0);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(o, table.getWinner());
    }
   @Test
    public void testRow5ByO() {
        Player x = new Player('x');
        Player o = new Player('o');
        Table table = new Table(o, x);
        table.setTable(0, 1);
        table.setTable(1, 1);
        table.setTable(2, 1);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(o, table.getWinner());
    } 
  @Test
    public void testRow6ByO() {
        Player x = new Player('x');
        Player o = new Player('o');
        Table table = new Table(o, x);
        table.setTable(0,2);
        table.setTable(1,2);
        table.setTable(2,2);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(o, table.getWinner());
    }   
     @Test
    public void testRow7ByO() {
        Player x = new Player('x');
        Player o = new Player('o');
        Table table = new Table(o, x);
        table.setTable(0, 0);
        table.setTable(1, 1);
        table.setTable(2,2);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(o, table.getWinner());
    }
     @Test
    public void testRow8ByO() {
        Player x = new Player('x');
        Player o = new Player('o');
        Table table = new Table(o, x);
        table.setTable(2, 2);
        table.setTable(1, 1);
        table.setTable(0,0);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(o, table.getWinner());
    }
     @Test
    public void testRow9ByO() {
        Player x = new Player('x');
        Player o = new Player('o');
        Table table = new Table(o, x);
        table.setTable(0, 0);
        table.checkWin();
        assertEquals(false, table.checkTable(0,0));
        assertEquals(o, table.getWinner());
    }
    @Test
    public void testRowByx() {
        Player x = new Player('x');
        Player o = new Player('o');
        Table table = new Table(x, o);
        table.setTable(0, 0);
        table.checkWin();
        assertEquals(false, table.checkTable(0,0));
        assertEquals(x, table.getWinner());
    }
     @Test
    public void testRowByo() {
        Player x = new Player('x');
        Player o = new Player('o');
        Table table = new Table(o, x);
        table.setTable(0, 0);
        table.setTable(0, 2);
        table.setTable(2, 0);
        table.setTable(2, 2);
        table.checkWin();
        assertEquals(false, table.checkTable(0,0));
        assertEquals(o, table.getWinner());
    }
    @Test
    public void testRowByX() {
        Player x = new Player('x');
        Player o = new Player('o');
        Table table = new Table(x, o);
        table.setTable(0, 0);
        table.setTable(0, 2);
        table.setTable(2, 0);
        table.setTable(2, 2);
        table.checkWin();
        assertEquals(false, table.checkTable(0,0));
        assertEquals(x, table.getWinner());
    }
}
